<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Đăng ký tân sinh viên</title>

    <link rel="stylesheet" href="interface.css">
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
    <?php
    $reg = "/^[0-9]{1,2}\\/[0-9]{1,2}\\/[0-9]{4}$/";
    $error = array();

    $target_dir = "upload/";
    $checkupload = 1;

    session_start();

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["fullName"])) {
            array_push($error, "Hãy nhập tên");
        }
        if (empty($_POST["gender"])) {
            array_push($error, "Hãy chọn giới tính");
        }
        if ($_POST["faculty"] == "0") {
            array_push($error, "Hãy chọn phân khoa");
        }
        if (empty($_POST["birthday"])) {
            array_push($error, "Hãy nhập ngày sinh");
        } elseif (!preg_match($reg, $_POST["birthday"])) {
            array_push($error, "Hãy nhập ngày sinh đúng định dạng");
        }

        if ($_FILES["uploadedImage"]["size"] > 0) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mimetype = finfo_file($finfo, $_FILES["uploadedImage"]["tmp_name"]);
            if ($mimetype == 'image/jpg' || $mimetype == 'image/jpeg' || $mimetype == 'image/gif' || $mimetype == 'image/png') {

                if (!is_dir($target_dir)) {
                    mkdir($target_dir, 0777, true);
                }

                $temp = explode(".", $_FILES["uploadedImage"]["name"]);
                $filename = $temp[0] . "_" . date('YmdHis') . "." . $temp[1];

                $target_file = $target_dir . basename($filename);

                if (move_uploaded_file($_FILES["uploadedImage"]["tmp_name"], $target_file)) {
                    $_POST["uploadedImage"] = $target_dir . $filename;
                }
            } else {
                array_push($error, "Nhập file có dạng *.JPG, *.JPEG, *.PNG");
            }
        }



        if (empty($error)) {
            $_SESSION["registry_data"] = $_POST;
            header("Location: submit.php");
        }
    }
    ?>

    <div class="my-container border-solid border-2 border-[#2f659c] py-6 px-12">
        <form action="" method="post" enctype="multipart/form-data">
            <?php
            if ($error) {
                foreach ($error as $e) {
                    echo "<p class='text-red-800'> $e </p>";
                }
                echo "<br>";
            }
            ?>
            <div class="flex mb-3">
                <div class="text-center float-left w-1/4">
                    <label class="
                            block
                            p-2
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#2f659c]
                            text-white" for="fullName">
                        Họ và tên <span class="text-red-800"> *</span>
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <input class="
                            w-3/4
                            h-full
                            ml-10
                            border-solid
                            border-2
                            border-[#2f659c]" type="text" id="fullName" name="fullName">

                </div>
            </div>

            <div class="flex mb-3">
                <div class="text-center float-left w-1/4">
                    <label class="
                            block
                            p-2
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#2f659c]
                            text-white">
                        Giới tính <span class="text-red-800"> *</span>
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <div class="w-3/4 h-full ml-12">
                        <?php
                        $gender = array("Nam", "Nữ");
                        for ($i = 0; $i < count($gender); $i++) {
                            echo "  <div class='inline-block mr-3 mt-2'>
                                                <input class='checked:bg-green-600' type='radio' name='gender' id='gender-$gender[$i]' value=$gender[$i]>
                                                <label class='ml-2' for='gender-$gender[$i]' class='margin-l5'>$gender[$i]</label>
                                            </div>
                                    ";
                        }
                        ?>
                    </div>
                </div>
            </div>

            <div class="flex mb-3">
                <div class="text-center float-left w-1/4">
                    <label class="
                            block
                            p-2
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#2f659c]
                            text-white" for="faculty">
                        Phân khoa <span class="text-red-800"> *</span>
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <div class="h-full">
                        <select class="w-1/2 h-full ml-10 border-solid border-2 border-[#2f659c]" name="faculty" id="faculty">
                            <?php
                            $faculty = array('0' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                            foreach ($faculty as $key => $value) {
                                echo "<option value='$key'>$value</option>";
                            }
                            ?>
                        </select>
                        <div class="arrow inline"></div>
                    </div>
                </div>
            </div>

            <div class="flex mb-3">
                <div class="text-center float-left w-1/4">
                    <label class="
                            block
                            p-2
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#2f659c]
                            text-white" for="birthday">
                        Ngày sinh <span class="text-red-800"> *</span>
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <div class="h-full">
                        <input datepicker class="
                                w-1/2
                                h-full
                                ml-10
                                border-solid
                                border-2
                                border-[#2f659c]" type="text" id="birthday" name="birthday" placeholder="dd/mm/yyyy">
                    </div>
                </div>
            </div>

            <div class="flex mb-3">
                <div class="text-center float-left w-1/4">
                    <label class="
                            block
                            p-2
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#2f659c]
                            text-white" for="address">
                        Địa chỉ
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <input class="
                            w-3/4
                            h-full
                            ml-10
                            border-solid
                            border-2
                            border-[#2f659c]" type="text" id="address" name="address">

                </div>
            </div>

            <div class="flex mb-3">
                <div class="text-center float-left w-1/4">
                    <label class="
                            block
                            p-2
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#2f659c]
                            text-white" for="uploadedImage">
                        Hình ảnh
                    </label>

                </div>
                <div class="float-left w-3/4 mt-2">
                    <input class="ml-10" type="file" accept=".jpg, .jpeg, .png" id="uploadedImage" name="uploadedImage">

                </div>
            </div>

            <div class="flex mt-8">
                <input class="
                        relative
                        left-[37.5%]
                        w-1/4
                        cursor-pointer
                        p-2
                        rounded-lg
                        border-solid
                        border-2
                        bg-[#1a4e8f]
                        border-[#2f659c]
                        text-white" type="submit" value="Đăng ký" name="submit">

            </div>


        </form>
    </div>

    <script src="https://unpkg.com/flowbite@1.5.3/dist/datepicker.js"></script>
</body>

</html>