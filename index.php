<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Danh sách</title>

    <link rel="stylesheet" href="interface.css">
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
    <?php
    $students = array(
        "Nguyễn Văn A" => "Khoa học máy tính",
        "Trần Thị B" => "Khoa học máy tính",
        "Nguyễn Hoàng C" => "Khoa học vật liệu",
        "Đinh Quang D" => "Khoa học vật liệu",
    );


    ?>

    <div class="my-container py-6 px-12">
        <form action="" method="post" enctype="multipart/form-data">


            <div class="flex mb-3">
                <div class="float-left w-1/4">
                    <label class="
                            block
                            p-2" for="faculty">
                        Khoa
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <div class="h-full">
                        <select class="w-3/4 h-full ml-10 border-solid border-2 border-[#4f85b4]" name="faculty" id="faculty">
                            <?php
                            $faculty = array('0' => '', 'MAT' => 'Khoa học máy tính', 'KDL' => 'Khoa học vật liệu');
                            foreach ($faculty as $key => $value) {
                                echo "<option value='$key'>$value</option>";
                            }
                            ?>
                        </select>
                        <div class="triangle inline"></div>
                    </div>
                </div>
            </div>

            <div class="flex mb-3">
                <div class="float-left w-1/4">
                    <label class="
                            block
                            p-2" for="fullName">
                        Từ khóa
                    </label>

                </div>
                <div class="float-left w-3/4">
                    <input class="
                            w-3/4
                            h-full
                            ml-10
                            border-solid
                            border-2
                            border-[#4f85b4]" type="text" id="fullName" name="fullName">

                </div>
            </div>

            <div class="flex mt-3">
                <div class="w-1/4"></div>
                <div class="w-3/4 float-right">
                    <input class="
                            relative
                            left-[37.5%]
                            w-1/4
                            cursor-pointer
                            p-2
                            rounded-lg
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#4f85b4]
                            text-white" type="submit" value="Tìm kiếm" name="submit">
                </div>

            </div>


        </form>

    </div>

    <div class="wrapper">
        <div class="flex justify-between items-center mx-auto mt-4">
            <div class="students-found">
                <span>Số sinh viên tìm thấy:</span>
                <span><?php echo count($students) ?></span>
            </div>
            <div class="add-button">
                <form action="add.php">
                    <input type="submit" class="
                            cursor-pointer
                            p-2
                            rounded-lg
                            border-solid
                            border-2
                            bg-[#1a4e8f]
                            border-[#4f85b4]
                            text-white
                            w-[100px]
                            mr-[90px]" value="Thêm">
                </form>
            </div>
        </div>

        <div class="mt-4">
            <table class="w-full">
                <thead>
                    <tr>
                        <th class="text-left">No</th>
                        <th class="text-left">Tên sinh viên</th>
                        <th class="text-left">Khoa</th>
                        <th class="text-left">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $order = 0;
                    foreach ($students as $name => $fac) {
                        $order++;
                        echo '<tr>
                                <td class="text-center">' . $order . '</td>
                                <td>' . $name . '</td>
                                <td>' . $fac . '</td>
                                <td class="action-table">
                                    <input
                                        type="submit"
                                        class="
                                        cursor-pointer
                                        px-3
                                        py-1
                                        mt-2
                                        border-solid
                                        border-2
                                        bg-[#1a4e8f]
                                        border-[#4f85b4]
                                        text-white"
                                    value="Xóa"
                                    >
                                    <input
                                        type="submit"
                                        class="
                                            cursor-pointer
                                            px-3
                                            py-1
                                            border-solid
                                            border-2
                                            bg-[#1a4e8f]
                                            border-[#4f85b4]
                                            text-white"
                                        value="Sửa"
                                    >
                                </td>
                            </tr>';
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

    <script src="https://unpkg.com/flowbite@1.5.3/dist/datepicker.js"></script>
</body>

</html>